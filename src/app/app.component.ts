import { Component } from '@angular/core';
import { MorsecodeService } from './morsecode.service';
import { Md5 } from '../../node_modules/ts-md5/dist/md5';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-pipe';
  morsecode: any;
  md5 = new Md5();
  constructor(private morseService: MorsecodeService) {
    this.morsecode = morseService.MorseList;
  }

  onSelect(): void {
    console.log(this.md5.appendStr('hello').end());
  }

}
