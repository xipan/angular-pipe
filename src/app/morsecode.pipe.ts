import { Pipe, PipeTransform } from '@angular/core';
import { MorsecodeService } from './morsecode.service';
import { Md5 } from '../../node_modules/ts-md5/dist/md5';



@Pipe({
  name: 'morsecode'
})
export class MorsecodePipe implements PipeTransform {

  morsecode: any;
  md5 = new Md5();

  constructor(private morseService: MorsecodeService) {
    this.morsecode = morseService.MorseList;
  }

  transform(value: any, codemode: string): any {
    let splitted = value.split('');
    let returnmorsecodes: string = '';
    if (codemode === 'morse') {
      splitted.forEach(function (value) {
        returnmorsecodes = returnmorsecodes + this.getmorsecodeafterchar(value)
      }, this)
    }
    if (codemode === 'md5') {
      returnmorsecodes = this.getmd5codeafterchar(value);
    }
    return returnmorsecodes;
  }

  getmorsecodeafterchar(charvalue: string): string {
    let returnmorsecode: string = '';
    this.morsecode.forEach(function (value) {
      if (value.symbol === charvalue) {
        returnmorsecode = value.morsecode;
        return false;
      }
    }, this);
    return returnmorsecode;
  }

  getmd5codeafterchar(stringvalue: string): string {
    return this.md5.appendStr(stringvalue).end().toString();
  }

}

