import { TestBed } from '@angular/core/testing';

import { MorsecodeService } from './morsecode.service';

describe('MorsecodeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MorsecodeService = TestBed.get(MorsecodeService);
    expect(service).toBeTruthy();
  });
});
